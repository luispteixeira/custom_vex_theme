"use strict";

function getExtraDOM(elem){
	var error = '<div class="icon error animateErrorIcon" style="display: none;"><span class="x-mark animateXMark"><span class="line left"></span><span class="line right"></span></span></div>';
	var warning = '<div class="icon warning" style="display: none;"> <span class="body"></span> <span class="dot"></span> </div>';
	var success = '<div class="icon success" style="display: none;"> <span class="line tip"></span> <span class="line long"></span> <div class="placeholder"></div> <div class="fix"></div> </div>';
	elem.append($(error + warning + success));
}

$( document ).ready(function() {
	
	vex.defaultOptions.className = 'vex-theme-besttables';
	
	vex.dialog.buildDialogForm = function(options) {
	    var $form, $input,$extraDOM, $message;
	    $form = $('<form class="vex-dialog-form"/>');
	    $extraDOM = getExtraDOM($form);
	    $message = $('<div class="vex-dialog-message" />');
	    $input = $('<div class="vex-dialog-input" />');
	    $form.append($message.append(options.message))
	    	 .append($input.append(options.input))
	    	 .append(vex.dialog.buttonsToDOM(options.buttons)).bind('submit.vex', options.onSubmit);
	    return $form;
	};

	$('.demo-confirm').on('click',function(e){
		e.preventDefault();

		vex.dialog.alert({
		    message: 'This is a vex dialog.',
		    contentClassName : $(this).data('class') +' center'
		});	
	})

});